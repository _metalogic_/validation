module bitbucket.org/_metalogic_/validation

go 1.13

require (
	bitbucket.org/_metalogic_/validator v0.0.0-20210601150159-9df48f02c435
	github.com/stretchr/testify v1.4.0
)
